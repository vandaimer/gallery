
## It's is a User model mock to simply the operations with the users.

class User:
    def __init__(self):
        self.users = [
            { 'id': 1, 'name': 'Bob', 'username': 'bob', 'password': 123 },
            { 'id': 2, 'name': 'Alice', 'username': 'alice', 'password': 123 }
        ]

    def get(self, username, password):
        for user in self.users:
            if user.get('username') == username and str(user.get('password')) == str(password):
                return user
        return None

    def get_by_id(self, id):
        for user in self.users:
            if user.get('id') == id:
                return user
        return None
