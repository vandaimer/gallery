from pyramid.response import Response
from pyramid.view import view_config
from boto.s3.connection import OrdinaryCallingFormat
import boto


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    # Get user from request.
    user = request.user

    # Get some settings
    to_review_prefix =  request.registry.settings.get('to_review_prefix')
    already_accepted_prefix = request.registry.settings.get('already_accepted_prefix')
    bucket_name =  request.registry.settings.get('storage.aws.bucket_name')

    # Get instance a S3 coonection
    conn = boto.connect_s3(calling_format=OrdinaryCallingFormat())
    # Se bucket name
    bucket = conn.lookup(bucket_name)

    # Choose the prefix
    # If user is logged in, he/she can be images that are not accepted yet and accept eac one.
    # If user is logged out, he/she can be images that are accepted.
    prefix = already_accepted_prefix
    if user:
      prefix = to_review_prefix

    # Get image list
    images = bucket.list(prefix=prefix)

    def reducer(image):
      return {
        'key': image.key,
        'url': image.generate_url(expires_in=0, query_auth=False),
      }

    images = list(map(reducer, images))

    return {'images': images, 'already_logged_in': True if user else False }
