from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import exception_response, HTTPSeeOther
from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.security import forget

from ..models.user import User

#This view do the logout.
@view_config(route_name='logout')
def logout_view(request):
    # Rewrite the headers to "logout" the user
    headers = forget(request)
    return HTTPSeeOther(request.route_url('home'), headers=headers)
