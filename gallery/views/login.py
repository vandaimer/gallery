from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import exception_response, HTTPSeeOther
from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.security import remember

#Import the User model mock
from ..models.user import User

#This view do the login
@view_config(route_name='login', request_method='POST')
def login_view(request):
    if 'form.login.submitted' in request.params:
        username = request.POST['username']
        password = request.POST['password']
        user = User().get(username, password)
        if user is not None:
            headers = remember(request, user.get('id'))
            return HTTPSeeOther(request.route_url('home'), headers=headers)
    return HTTPSeeOther(request.route_url('home'))
