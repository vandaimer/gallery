from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import exception_response, HTTPSeeOther
from pyramid_storage.exceptions import FileNotAllowed
from time import time

#This view do the upload the image submited
@view_config(route_name='upload_image', request_method='POST',)
def update_image_view(request):
    #If the POST haven't file key, redirect to home
    if 'file' not in request.POST:
     return HTTPSeeOther(request.route_url('home'))

    #Get to_review_prefix from settings. (production.ini, development.ini)
    to_review_prefix =  request.registry.settings.get('to_review_prefix')

    # Make a new file name with the to_review_prefix
    filename = to_review_prefix + str(int(time())) + '.png'

    # Send to S3
    request.storage.save_file(request.POST['file'].file, extensions=('png', 'jpg', 'jpeg'), filename=filename)

    # Redict to home
    return HTTPSeeOther(request.route_url('home'))
