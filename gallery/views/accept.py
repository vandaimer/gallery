from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import exception_response, HTTPSeeOther
from pyramid_storage.exceptions import FileNotAllowed
from time import time
import boto3

# The view accept some image if user is logged in
@view_config(route_name='accept_image')
def accept_image_view(request):
    # Check if has key in GET request and user is logged in
    if 'key' not in request.GET or not request.GET.get('key') or not request.user:
     return HTTPSeeOther(request.route_url('home'))

    # Get some settings
    bucket_name =  request.registry.settings.get('storage.aws.bucket_name')
    to_review_prefix =  request.registry.settings.get('to_review_prefix')
    already_accepted_prefix =  request.registry.settings.get('already_accepted_prefix')

    # Get key from GET request
    key = request.GET.get('key')
    # Change the prefix name of image
    new_key = key.replace(to_review_prefix, already_accepted_prefix)

    # Here I use boto3 because it's easier
    # The better think is not use pyramid-storage and use just boto3, but that way I can show my knowledge about it
    s3 = boto3.resource('s3')

    # Do the copy image. Image with TO_REVIEW_ prefix to image with ALREADY_ACCEPTED_ prefix.
    s3.Object(bucket_name, new_key).copy_from(CopySource='%s/%s' % (bucket_name, key), ContentType='image/png', ACL='public-read')

    #Delete the image that has TO_REVIEW_ prefix
    s3.Object(bucket_name, key).delete()

    return HTTPSeeOther(request.route_url('home'))
