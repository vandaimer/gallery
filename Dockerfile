FROM python:3.6

RUN mkdir -p /app/gallery

WORKDIR /app
COPY . .

RUN pip install -r /app/requires.txt
RUN pip install -e ".[testing]"

EXPOSE 6543
CMD ["pserve", "development.ini", "--reload"]
