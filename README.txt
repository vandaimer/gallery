# Gallery

I trying simply all that things that were possible to can delivery some code that shows my skills. For I do it better, I need more time. My best skills are in backend feature, so, in the frontend, I can do, but not good as a frontender.

# What was not done
- Users must be able to like photos.
- Users should be able to sort the photos by total of likes or by date taken.
- Unit test
- Dockerfile & docker-compose
-- I started it but I did not finish it

# How I start the project
- I was used this [template](https://github.com/Pylons/pyramid-cookiecutter-alchemy "template") to create the Pyramid project.
- I was used this [template](https://templated.co/snapshot "template") to create the design.

# What was done
- Image upload to S3
- Image approvation by the man or women
- Show imagens that was accepted

# Some explanations about the code
- To control which imagen was accepted and which wasn't, I used **prefix**.
-- When *TO_REVIEW_*  prefix is to images that need an approval and *ALREADY_ACCEPTED_*

# How you can test
- http://159.65.99.15 (Using digitalocean)
-- Using nginx with reverse proxy
- I possible do loggin with **bob** or **alice**
-- *username* **bob** | *password* **123**
-- *username* **alice** | *password* **123**